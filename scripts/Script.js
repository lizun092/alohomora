﻿var cloneCount = 1;
$(document).ready(function () {
    $("#EditPopUpModal").hide();
    //  var elementGenerator = '<input class="form-control" name="ContactNumber" placeholder="Enter Your Number" type="text" value=""><span class="col-md-8 error"></span>';

    $("#AddContact").click(function () {
        $("#CloneItem").clone().attr('id', '' + cloneCount).append('<input class="form-control" name="ContactNumber" placeholder="Enter Your Number" type="text" value="" id="AltContactNumbers' + cloneCount + '" oninput="contactValid(' + cloneCount + ')"><span class="col-md-8 error" id="errorMessage' + cloneCount + '"></span>').insertBefore("#AppendItem");
        cloneCount++;
    });
    $("#ContactNumber").on("input", function () {

        var firstContactData = $("#ContactNumber").val();
        var firstError = "#FirstErrorContactNumber";
        checkContact(firstContactData, firstError);
    });

});
function saveEditData() {
    var id = $('#IdPopUp').val();
    var fname = $('#FirstNamePopUp').val();
    var lname = $('#LastNamePopUp').val();
    var address = $('#AddressPopUp').val();
    var editData = {
        Id: id,
        FirstName: fname,
        LastName: lname,
        Address: address
    }
    $.ajax({
        type: 'POST',
        url: "/Home/UserEdit",
        data: JSON.stringify(editData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if(data=="1")
            {
                $("#EditPopUpModal").hide();
                alert("Edited successfully");
            }
            else
            {
                alert("Not edited");
            }

        },
        error: function (a, jqXHR, exception) { }
    })
}
function editUser(data) {
    //var obj = JSON.parse(data);
    $("#EditPopUpModal").show();
    $('#IdPopUp').val(data.Id);
    $('#FirstNamePopUp').val(data.FirstName);
    $('#LastNamePopUp').val(data.LastName);
    $('#AddressPopUp').val(data.Address);
}
function closeUserModal() {
    $("#EditPopUpModal").hide();
}

function userDelete(id) {
    if (confirm("Are you sure?"))
    {
        $.ajax({
            type: "POST",
            url: "/Home/UserDelete?id=" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response == "1")
                {
                    alert("Deleted Successfully");
                }
               
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        })
    }
}

function checkContact(data, error) {
    var regex = /^[0-9]*$/;
    if (!(regex.test(data))) {
        $(error).html("Enter only numbers");
    }
    else {
        $(error).html("");
    }
}
function contactValid(cloneCount) {
    for (var i = 1; i <= cloneCount; i++) {
        var contactValue = $("#AltContactNumbers" + i).val();
        var message = "#errorMessage" + i;
        checkContact(contactValue, message);
    }
}
