﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlohoMora.UserModelContext;
using AlohoMora.ViewModels;

namespace AlohoMora.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult AdminIndex(int? id)
        {
            try
            {
                if (Session["password"] != null)
                {
                    using (UserAccountContext userAccount = new UserAccountContext())
                    {

                        var obj = userAccount.UserDetails.Find(id);
                        var userContactData = userAccount.RelationshipHolds.Where(m => m.Id == obj.Id).Include(m => m.UserContactData).ToList();
                        var data = new UserDetailsView
                        {
                            Id = obj.Id,
                            FirstName = obj.FirstName,
                            LastName = obj.LastName,
                            Mobile = obj.Mobile,
                            Email = obj.Email,
                            Address = obj.Address,
                            ImageUrl = obj.ImageUrl,
                            AlternetText = obj.AlternetText,
                            UserContactList = userContactData,
                            UserRoleId = obj.RoleId
                        };
                        return View(data);
                    }
                }
                return RedirectToAction("UserLogin", "Login");
            }
            catch (Exception e)
            {
                return RedirectToAction("UserLogin", "Login");
            }
        }
    }
}