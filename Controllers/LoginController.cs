﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlohoMora.ViewModels;
using AlohoMora.UserModelContext;
using System.Net.Mail;
using System.Net;
using AlohoMora.Models;

namespace AlohoMora.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult UserLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserLogin(UserLogin userLogin)
        {
            if (!ModelState.IsValid)
            {
                return View(userLogin);
            }
            using (UserAccountContext obj = new UserAccountContext())
            {
                string pass = PasswordClass.Encrypt(userLogin.Password);
                var userData = obj.UserDetails.FirstOrDefault(x => x.Email == userLogin.EmailId && x.Password == pass);
                if (userData != null && userData.RoleId==1)
                {
                    Session["admin"] = 1;
                    Session["email"] = userLogin.EmailId;
                    Session["password"] = pass;
                    ViewBag.messageLogin = "Login successful";
                    return RedirectToAction("AdminIndex", "Admin", new { id = userData.Id});
                }
                else if (userData != null && userData.RoleId != 1)
                {
                    Session["admin"] = 2;
                    Session["email"] = userLogin.EmailId;
                    Session["password"] = pass;
                    ViewBag.messageLogin = "Login successful";
                    return RedirectToAction("UserDetails", "Home", new { id = userData.Id });
                }
                else
                {
                    ViewBag.messageLogin = "Invalid email or password";
                }
            }
            return View();
        }
        public ActionResult UserLogout()
        {
            Session["email"] = null;
            Session["password"] = null;
            return RedirectToAction("UserLogin","Login");
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(ForgetPasswordModel userAccount)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(userAccount);
                }
                string message = "";
                bool status = false;
                Random random = new Random();
                int value = random.Next(10000);
                SendVarificationCode(userAccount.EmailId, value);
                message = "code has been sent to your email id : " + userAccount.EmailId;
                status = true;
                ModelState.Clear();
                TempData["code"] = value;
                Session["verifyEmail"] = userAccount.EmailId;
                ViewBag.successMessage = message;
                ViewBag.status = status;
                return View(userAccount);
            }
            catch(Exception e)
            {
                return RedirectToAction("UserLogin", "Login");
            }
            
        }
        [NonAction]
        public void SendVarificationCode(string emailId, int activationCode)
        {
            try
            {
                var verifyCode = "" + activationCode;
                var fromEmail = new MailAddress("mindfiresolutionspurnendu@gmail.com", "DotNet MVC");
                var toEmail = new MailAddress(emailId);
                var fromEmailPassword = "mindfire@123";
                string body = "Your Code <strong>" + verifyCode + "</strong>";
                string subject = "Change password code";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                };
                using (var message = new MailMessage(fromEmail, toEmail)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                    smtp.Send(message);
            }
            catch (Exception e)
            {
                RedirectToAction("UserLogin", "Login");
            }
           
        }
        
        [HttpPost]
        public ActionResult GeneratePassword(UserRegistration userAccount)
        {
            try
            {
                if (userAccount.Code != int.Parse(TempData["code"].ToString()))
                {
                    return RedirectToAction("ForgotPassword", "Login");
                }
                return View();
            }
            catch(Exception e)
            {
                return RedirectToAction("UserLogin", "Login");
            }
            
        }
    }
}