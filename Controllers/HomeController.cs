﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AlohoMora.ViewModels;
using AlohoMora.Models;
using AlohoMora.UserModelContext;
using System.Data;
using System.Data.Entity;
using AlohoMora.CustomFilters;

namespace AlohoMora.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [CustomAdminAuth]
        [HttpGet]
        public ActionResult Details()
        {
            
            try
            {
                //if (Session["password"] != null&& int.Parse(Session["admin"].ToString())==1)
                //{
                    using (UserAccountContext userAccount = new UserAccountContext())
                    {
                        return View(userAccount.UserDetails.ToList<User>());
                    }
                //}
                //return RedirectToAction("UserLogin", "Login");
            }
            catch (Exception e)
            {
                return RedirectToAction("UserLogin", "Login");
            }
            
        }
        
        public ActionResult UserDetails(int? id)
        {
            try
            {
                if(id!=null)
                {
                    if (Session["email"] != null && Session["password"] != null)
                    {
                        using (UserAccountContext userAccount = new UserAccountContext())
                        {
                            var obj = userAccount.UserDetails.Find(id);
                            var userContactData = userAccount.RelationshipHolds.Where(m => m.Id == obj.Id).Include(m => m.UserContactData).ToList();
                            var userDetailsObj = new UserDetailsView
                            {
                                Id = obj.Id,
                                FirstName = obj.FirstName,
                                LastName = obj.LastName,
                                Mobile = obj.Mobile,
                                Email = obj.Email,
                                Address = obj.Address,
                                ImageUrl = obj.ImageUrl,
                                AlternetText = obj.AlternetText,
                                UserContactList = userContactData
                            };
                            return View(userDetailsObj);
                        }
                    }
                    return RedirectToAction("UserLogin", "Login");
                }
                
                return RedirectToAction("UserLogin", "Login");
            }
            catch(Exception e)
            {
                return RedirectToAction("UserLogin", "Login");
            }
        }
        [HttpPost]
        public ActionResult UserEdit(UserRegistration userData)
        {
            if(userData!=null)
            {
                using (UserAccountContext userAccount = new UserAccountContext())
                {
                    var editDetails = userAccount.UserDetails.Find(userData.Id);
                    if (editDetails != null)
                    {
                        editDetails.FirstName = userData.FirstName;
                        editDetails.LastName = userData.LastName;
                        editDetails.Address = userData.Address;
                        userAccount.SaveChanges();
                        return Json("1");
                    }
                    return Json("0");
                }
            }
            return RedirectToAction("UserLogin", "Login");
        }
        [HttpPost]
        public ActionResult UserDelete(int? id)
        {
            if(id!=null)
            {
                // var id = int.Parse(Request["id"]);
                using (UserAccountContext userAccount = new UserAccountContext())
                {
                    var deleteUser = userAccount.UserDetails.Find(id);
                    var editUserData = userAccount.RelationshipHolds.Where(m => m.Id == id);
                    foreach (var data in editUserData)
                    {
                        userAccount.RelationshipHolds.Remove(data);
                    }
                    userAccount.UserDetails.Remove(deleteUser);
                    userAccount.SaveChanges();
                    return Json("1");
                }
            }
            return Json("0");
        }
    }
}
