namespace AlohoMora.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AlohoMora.UserModelContext.UserAccountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AlohoMora.UserModelContext.UserAccountContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            List<UserContactType> addContact = new List<UserContactType>();
            addContact.Add(new UserContactType() { Type = "alternetMobile" });
            addContact.Add(new UserContactType() { Type = "home" });
            addContact.Add(new UserContactType() { Type = "office" });
            addContact.Add(new UserContactType() { Type = "fax" });

            context.UserContactDetails.AddRange(addContact);

            List<Role> addRole = new List<Role>();
            addRole.Add(new Role() { RoleType = "Admin" });
            addRole.Add(new Role() { RoleType = "User" });

            context.RoleDetails.AddRange(addRole);
        }
    }
}
