﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlohoMora.ViewModels;
using AlohoMora.Models;
using AlohoMora.UserModelContext;

namespace AlohoMora.BAL
{
    public class BusnissesLogics
    {
        //Here user datas are uploaded in side database
        public static User UploadUserData(UserAccountContext userDB, UserRegistration userAccount, string imageUrl)
        {
            var userModel = new User
            {
                ActivationCode = Guid.NewGuid(),
                ImageUrl = imageUrl,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                Password = PasswordClass.Encrypt(userAccount.Password),
                Email = userAccount.EmailId,
                Address = userAccount.Address,
                Mobile = userAccount.Mobile,
                AlternetText = userAccount.FirstName + " " + userAccount.LastName + "'s Photo",
                RoleId = 2
            };
            userDB.UserDetails.Add(userModel);
            userDB.SaveChanges();
            return userModel;
        }
    }
}