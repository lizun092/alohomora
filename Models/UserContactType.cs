﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AlohoMora.Models
{
    public class UserContactType
    {
        [Key]
        public int ContactId { get; set; }
        [Required]
        public string Type { get; set; }
    }
}