﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AlohoMora.CustomFilters
{
    public class CustomAdminAuth:ActionFilterAttribute,IActionFilter
    {
        //private int adminAllowed;
        //public CustomAdminAuth(int adminRoleId)
        //{
        //    adminAllowed = adminRoleId;
       

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(HttpContext.Current.Session["password"]==null || int.Parse(HttpContext.Current.Session["admin"].ToString())==2)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"Controller" ,"Login"},
                    {"Action" ,"UserLogin"}
                });
            }
            base.OnActionExecuting(filterContext);
        }
       
    }
}