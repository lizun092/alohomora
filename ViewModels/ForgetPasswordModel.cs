﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlohoMora.ViewModels
{
    public class ForgetPasswordModel
    {
        [DisplayName("Email Id *")]
        [Required(ErrorMessage = "You must provide a email address")]
        [EmailAddress]
        public string EmailId { get; set; }
        public int Code { get; set; }
    }
}