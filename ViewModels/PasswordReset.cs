﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlohoMora.ViewModels
{
    public class PasswordReset
    {
        [Required]
        [EmailAddress]
        public string EmailId { get; set; }
    }
}