﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AlohoMora.ViewModels;
using AlohoMora.Models;

namespace AlohoMora.UserModelContext
{
    public class UserAccountContext : DbContext
    {
        public UserAccountContext() : base("UserAccount")
        {

        }
        public DbSet<Role> RoleDetails { get; set; }
        public DbSet<User> UserDetails { get; set; }
        public DbSet<UserContactType> UserContactDetails { get; set; }
        public DbSet<UserContact> RelationshipHolds { get; set; }
    }
}